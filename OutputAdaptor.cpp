/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "Global.hpp"
#include "OutputAdaptor.hpp"

#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

OutputAdaptor::OutputAdaptor( QObject *parent ) : QDBusAbstractAdaptor( parent ) {
    setAutoRelaySignals( true );
}


OutputAdaptor::~OutputAdaptor() {
}


/** Show the desktop of the current workspace of the current output */
void OutputAdaptor::ShowDesktop() {
    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    mShowingDesktop = not mShowingDesktop;

    idle_call->run_once(
        [ = ] () {
            wf::output_t *output = core.seat->get_active_output();

            if ( output == nullptr ) {
                return;
            }

            /** We just flipped to showing the desktop. Do the action. */
            if ( mShowingDesktop ) {
                /** Get the views of the current workspace in the stacking order */
                std::vector<wayfire_toplevel_view> views = output->wset()->get_views( wf::WSET_CURRENT_WORKSPACE | wf::WSET_SORT_STACKING );

                /** Reverse the order */
                std::reverse( views.begin(), views.end() );

                for ( wayfire_toplevel_view view: views ) {
                    /** Ignore already minimized views */
                    if ( view->minimized ) {
                        continue;
                    }

                    /** Store the minimize data hint */
                    view->store_data( std::make_unique<wf::custom_data_t>(), "dbusqt-showdesktop" );

                    /** Request a minimize */
                    core.default_wm->minimize_request( view, true );
                }
            }

            /** We just flipped to showing the windows. Do the action. */
            else {
                /** Get the views of the current workspace in the stacking order */
                std::vector<wayfire_toplevel_view> views = output->wset()->get_views( wf::WSET_CURRENT_WORKSPACE | wf::WSET_SORT_STACKING );

                /** Reverse the order */
                std::reverse( views.begin(), views.end() );

                for ( wayfire_toplevel_view view: views ) {
                    /** Process only if it was minimized by this plugin */
                    if ( view->has_data( "dbusqt-showdesktop" ) ) {
                        /** Erase the minimize data hint */
                        view->erase_data( "dbusqt-showdesktop" );

                        /** Request an un-minimize */
                        core.default_wm->minimize_request( view, false );
                    }
                }
            }
        }
    );
}


/** List all the connected output IDs */
QUIntList OutputAdaptor::QueryOutputIds() {
    QUIntList outputs;

    for ( wf::output_t *output: wf_outputs ) {
        outputs << output->get_id();
    }

    return outputs;
}


/** Query the currently active output */
uint OutputAdaptor::QueryActiveOutput() {
    uint output_id = core.seat->get_active_output()->get_id();

    return output_id;
}


/** Query the workspace grid size of the output @output_id */
WorkSpaceGrid OutputAdaptor::QueryOutputWorkspaceGrid( uint output_id ) {
    wf::output_t  *output = get_output_from_id( output_id );
    WorkSpaceGrid wsg;

    if ( output ) {
        wf::dimensions_t grid = output->wset()->get_workspace_grid_size();
        wsg = WorkSpaceGrid{ grid.height, grid.width };
    }

    return wsg;
}


/** Query the current workspace of the output @output_id */
WorkSpace OutputAdaptor::QueryOutputWorkspace( uint output_id ) {
    wf::output_t *output = get_output_from_id( output_id );
    WorkSpace    ws;

    if ( output ) {
        wf::point_t curWS = output->wset()->get_current_workspace();
        ws = WorkSpace{ curWS.y, curWS.x };
    }

    return ws;
}


/** Change the workspace of the given output @output_id */
void OutputAdaptor::ChangeWorkspace( uint output_id, WorkSpace ws ) {
    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    idle_call->run_once(
        [ = ] () {
            wf::output_t *output = get_output_from_id( output_id );

            if ( output ) {
                output->wset()->request_workspace( { ws.column, ws.row } );
            }

            delete idle_call;
        }
    );
}


/** List all the views on the current output */
QUIntList OutputAdaptor::QueryActiveOutputViews() {
    uint output_id = core.seat->get_active_output()->get_id();

    return QueryOutputViews( output_id );
}


/** List all the views on the output @output_id.
 *  The views should be top-level and mapped */
QUIntList OutputAdaptor::QueryOutputViews( uint output_id ) {
    QUIntList views;

    wf::output_t *output = get_output_from_id( output_id );

    if ( output ) {
        std::vector<wayfire_toplevel_view> toplevels = output->wset()->get_views();
        for ( wayfire_toplevel_view view: toplevels ) {
            views << view->get_id();
        }
    }

    return views;
}


/** Present windows of the current workspace of the current output */
void OutputAdaptor::PresentWorkspaceViews( uint output_id ) {
    /** To be replaced with IPC calls */
    Q_UNUSED( output_id );
}


/** Present windows of all workspaces of the current output */
void OutputAdaptor::PresentOutputViews( uint output_id ) {
    /** To be replaced with IPC calls */
    Q_UNUSED( output_id );
}
