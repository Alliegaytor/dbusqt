project(
	'wayfire-plugin-dbusqt',
	'c',
	'cpp',
	version: '0.1',
	license: 'MIT',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

wayfire  = dependency('wayfire')
wlroots  = dependency('wlroots')
wfconfig = dependency('wf-config')

QtDeps = dependency(
	'qt6',
	modules: [ 'Core', 'DBus' ],
	required: false,
)

if not QtDeps.found()
	QtDeps = dependency(
		'qt5',
		modules: [ 'Core', 'DBus' ],
		required: false,
	)

	if not QtDeps.found()
		error( 'DBusQt plugin requires either Qt5 or Qt6 installed.' )

	# Qt5 found
	else

		Qt = import( 'qt5' )
	endif

# Qt6 found
else

	Qt = import( 'qt6' )
endif

Mocs = Qt.compile_moc(
 	headers : [
		'dbus.hpp',
		'MiscAdaptor.hpp',
		'OutputAdaptor.hpp',
		'WorkspaceAdaptor.hpp',
		'ViewsAdaptor.hpp',
	],
 	dependencies: QtDeps
)

add_project_arguments(['-DWLR_USE_UNSTABLE'], language: ['cpp', 'c'])
add_project_arguments(['-DWAYFIRE_PLUGIN'], language: ['cpp', 'c'])
add_project_link_arguments(['-rdynamic','-fPIC'], language:'cpp')

dbusqt = shared_module(
  'dbusqt', [
	'dbus.cpp',
	'MiscAdaptor.cpp',
	'OutputAdaptor.cpp',
	'WorkspaceAdaptor.cpp',
	'ViewsAdaptor.cpp',
	'dbustypes.cpp',
	Mocs,
  ],

  dependencies: [ wlroots, wfconfig, QtDeps ],

  install: true,
  install_dir: join_paths( get_option( 'libdir' ), 'wayfire' )
)

install_data( 'dbusqt.xml', install_dir: wayfire.get_variable( pkgconfig: 'metadatadir' ) )

summary = [
	'',
	'----------------',
	'wayfire-plugin-dbusqt @0@'.format(meson.project_version()),
	'----------------',
	''
]
message('\n'.join(summary))
